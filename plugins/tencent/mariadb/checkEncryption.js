var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'MariaDB Encryption Status',
    category: 'MariaDB',
    domain: 'Databases',
    description: 'Enable encryption to protect data at rest and in transit.',
    more_info: 'Verify that the "IsEncryptSupported" field in the DescribeDBInstances response is set to 1 (enabled) to ensure data encryption is activated.',
    recommended_action: 'Enable data encryption for the MariaDB instances.',
    link: 'https://intl.cloud.tencent.com/document/product/557/42989',
    apis: ['MARIADB:describeDBInstances'],
    compliance: {
        hipaa: 'HIPAA requires that all data is encrypted, including data at rest. ' +
                'MariaDB encryption helps meet this requirement by providing automated encryption ' +
                'of data at rest.'
    },

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeDBInstances = helpers.addSource(cache, source, ['mariadb', 'describeDBInstances', region]);

        if (!describeDBInstances) return callback(null, results, source);

        if (describeDBInstances.err || !describeDBInstances.data) {
            helpers.addResult(results, 3, `Unable to query for MariaDB instances: ${helpers.addError(describeDBInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeDBInstances.data || !describeDBInstances.data.length) {
            helpers.addResult(results, 0, 'No MariaDB instances found', region);
            return callback(null, results, source);
        }

        async.each(describeDBInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (instance.IsEncryptSupported && instance.IsEncryptSupported === 1) {
                helpers.addResult(results, 0, 'Data encryption is enabled', region, instance.InstanceId);
            } else {
                helpers.addResult(results, 2, 'Data encryption is not enabled', region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
