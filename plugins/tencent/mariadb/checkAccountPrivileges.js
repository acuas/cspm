var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Account Privileges Review',
    category: 'MariaDB',
    domain: 'Database',
    description: 'Regularly review and manage user privileges to limit access to the minimum required level.',
    more_info: 'Examine the "UserName" and "Privileges" fields to ensure that user accounts have appropriate privileges and permissions. Identify and remove any unnecessary privileges.',
    recommended_action: 'Check the user account privileges and make sure they are at the minimum required level.',
    link: 'https://intl.cloud.tencent.com/document/product/237/3848',
    apis: ['MARIADB:describeAccounts','MARIADB:describeAccountPrivileges'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeDBAccountPrivileges = helpers.addSource(cache, source, ['mariadb', 'describeAccountPrivileges', region]);

        if (!describeDBAccountPrivileges) return callback(null, results, source);

        if (describeDBAccountPrivileges.err || !describeDBAccountPrivileges.data) {
            helpers.addResult(results, 3, `Unable to query for MariaDB account privileges: ${helpers.addError(describeDBAccountPrivileges)}`, region);
            return callback(null, results, source);
        }

        if (!describeDBAccountPrivileges.data || !describeDBAccountPrivileges.data.length) {
            helpers.addResult(results, 0, 'No MariaDB user accounts found', region);
            return callback(null, results, source);
        }

        async.each(describeDBAccountPrivileges.data, (account, cb) => {
            if (!account.UserName) return cb();

            // Define your minimum required privileges here
            let requiredPrivileges = ["SELECT", "UPDATE"];

            let accountPrivileges = account.Privileges;
            let excessPrivileges = accountPrivileges.filter(p => !requiredPrivileges.includes(p));

            if (excessPrivileges.length === 0) {
                helpers.addResult(results, 0, 'User account has minimum required privileges', region, account.UserName);
            } else {
                helpers.addResult(results, 2, `User account has excess privileges: ${excessPrivileges.join(', ')}`, region, account.UserName);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
