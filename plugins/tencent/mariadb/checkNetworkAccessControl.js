var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'MariaDB Network Access Control',
    category: 'MariaDB',
    domain: 'Databases',
    description: 'Restrict network access to the MariaDB instance.',
    more_info: 'Review the "Inbound" and "Outbound" rules in the DescribeDBSecurityGroups response to ensure that only necessary and authorized IP addresses and ports are allowed.',
    recommended_action: 'Review the network access rules for the MariaDB instances and ensure they are restricted to only necessary and authorized IP addresses and ports.',
    link: 'https://intl.cloud.tencent.com/document/product/237/15807',
    apis: ['MARIADB:describeDBInstances', 'MARIADB:describeDBSecurityGroups'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeDBSecurityGroups = helpers.addSource(cache, source, ['mariadb', 'describeDBSecurityGroups', region]);

        if (!describeDBSecurityGroups) return callback(null, results, source);

        if (describeDBSecurityGroups.err || !describeDBSecurityGroups.data) {
            helpers.addResult(results, 3, `Unable to query for MariaDB security groups: ${helpers.addError(describeDBSecurityGroups)}`, region);
            return callback(null, results, source);
        }

        if (!describeDBSecurityGroups.data || !describeDBSecurityGroups.data.length) {
            helpers.addResult(results, 0, 'No MariaDB security groups found', region);
            return callback(null, results, source);
        }

        async.each(describeDBSecurityGroups.data, (securityGroup, cb) => {
            if (!securityGroup.SecurityGroupId) return cb();

            // Define authorized IP addresses and ports
            const authorizedIps = ['192.168.0.0/16'];  // Adjust as needed
            const authorizedPorts = ['3306'];  // Adjust as needed

            securityGroup.Inbound.forEach(rule => {
                if (!authorizedIps.includes(rule.CidrIp) || !authorizedPorts.includes(rule.PortRange)) {
                    helpers.addResult(results, 2, `Inbound rule is not restricted to necessary IPs and ports: ${rule.CidrIp} - ${rule.PortRange}`, region, securityGroup.SecurityGroupId);
                } else {
                    helpers.addResult(results, 0, 'Inbound rule is restricted to necessary IPs and ports', region, securityGroup.SecurityGroupId);
                }
            });

            securityGroup.Outbound.forEach(rule => {
                if (!authorizedIps.includes(rule.CidrIp) || !authorizedPorts.includes(rule.PortRange)) {
                    helpers.addResult(results, 2, `Outbound rule is not restricted to necessary IPs and ports: ${rule.CidrIp} - ${rule.PortRange}`, region, securityGroup.SecurityGroupId);
                } else {
                    helpers.addResult(results, 0, 'Outbound rule is restricted to necessary IPs and ports', region, securityGroup.SecurityGroupId);
                }
            });

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
