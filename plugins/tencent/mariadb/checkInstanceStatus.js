var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'MariaDB Instance Status Check',
    category: 'MariaDB',
    domain: 'Databases',
    description: 'Ensure that the MariaDB instance is in a stable and healthy state.',
    more_info: 'Verify that the "Status" field in the DescribeDBInstances response is set to a valid and expected value.',
    recommended_action: 'Check the MariaDB instances and make sure they are in a stable and healthy state.',
    link: 'https://intl.cloud.tencent.com/document/product/237/2025',
    apis: ['MARIADB:describeDBInstances'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeDBInstances = helpers.addSource(cache, source, ['mariadb', 'describeDBInstances', region]);

        if (!describeDBInstances) return callback(null, results, source);

        if (describeDBInstances.err || !describeDBInstances.data) {
            helpers.addResult(results, 3, `Unable to query for MariaDB instances: ${helpers.addError(describeDBInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeDBInstances.data || !describeDBInstances.data.length) {
            helpers.addResult(results, 0, 'No MariaDB instances found', region);
            return callback(null, results, source);
        }

        async.each(describeDBInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            // Define the valid and expected status
            const validStatuses = [2];  // Running
            if (validStatuses.includes(instance.Status)) {
                helpers.addResult(results, 0, 'MariaDB instance is in a stable and healthy state', region, instance.InstanceId);
            } else {
                helpers.addResult(results, 2, `MariaDB instance is not in a stable state: Status - ${instance.Status}`, region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
