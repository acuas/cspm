var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance Disk Encryption and Type',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that disk encryption is enabled and disk type is as expected on all CVM instances.',
    more_info: 'Disk encryption enhances the security of data at rest. The disk type should be as per the organizational standards.',
    recommended_action: 'Enable disk encryption and set the disk type as expected on all CVM instances.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances'],
    compliance: {
        hipaa: 'HIPAA requires that all data is encrypted, including data at rest. ' +
                'CVM disk is a HIPAA-compliant solution that provides automated encryption ' +
                'of CVM instance data at rest.',
        pci: 'PCI requires proper encryption of cardholder data at rest. Encryption ' +
             'should be enabled for all disk volumes storing this type of data.'
    },
    settings: {
        expected_disk_type: {
            name: 'Expected Disk Type',
            description: 'The expected disk type for CVM instances',
            regex: '^.*$',
            default: 'CLOUD_SSD'
        }
    },

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';
        let expected_disk_type = settings.expected_disk_type || this.settings.expected_disk_type.default;

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);

        if (!describeInstances) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data || !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (!instance.SystemDisk || !instance.SystemDisk.Encrypt || instance.SystemDisk.DiskType !== expected_disk_type) {
                helpers.addResult(results, 2, 'CVM instance system disk does not meet the requirements', region, instance.InstanceId);
            }

            if (instance.DataDisks) {
                for (let dataDisk of instance.DataDisks) {
                    if (!dataDisk.Encrypt || dataDisk.DiskType !== expected_disk_type) {
                        helpers.addResult(results, 2, 'CVM instance data disk does not meet the requirements', region, instance.InstanceId);
                        break;
                    }
                }
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
