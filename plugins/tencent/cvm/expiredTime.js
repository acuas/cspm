var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance ExpiredTime',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that CVM instances are not running beyond their ExpiredTime.',
    more_info: 'Running instances beyond their ExpiredTime could indicate a misconfiguration or potentially unneeded cost.',
    recommended_action: 'Terminate or renew CVM instances that are running beyond their ExpiredTime.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);

        if (!describeInstances) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data || !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        let now = new Date();

        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (instance.ExpiredTime) {
                let expiredTime = new Date(instance.ExpiredTime);

                if (now > expiredTime) {
                    helpers.addResult(results, 2, 'CVM instance is running beyond its ExpiredTime', region, instance.InstanceId);
                } else {
                    helpers.addResult(results, 0, 'CVM instance is not running beyond its ExpiredTime', region, instance.InstanceId);
                }
            } else {
                helpers.addResult(results, 0, 'CVM instance is not running beyond its ExpiredTime', region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
