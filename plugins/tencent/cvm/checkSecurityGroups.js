var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance Public Exposure and Security Group Rules',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that CVM instances are not exposed publicly and that associated security group(s) have appropriate rules.',
    more_info: 'To reduce the potential attack surface, your Tencent CVM instances should not have public IP addresses and should have associated security group(s) with secure rules.',
    recommended_action: 'Modify CVM instances to remove public IP addresses and update security group rules.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances', 'VPC:describeSecurityGroups', 'VPC:describeSecurityGroupPolicies'],
    compliance: {
        hipaa: 'HIPAA requires strict access controls to networks and services ' +
                'processing sensitive data. Security groups are the built-in ' +
                'method for restricting access to Tencent services and should be ' +
                'configured to allow least-privilege access.',
        pci: 'PCI has explicit requirements around firewalled access to systems. ' +
             'Security groups should be properly secured to prevent access to ' +
             'backend services.'
    },

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);
        let describeSecurityGroups = helpers.addSource(cache, source, ['vpc', 'describeSecurityGroups', region]);
        let describeSecurityGroupPolicies = helpers.addSource(cache, source, ['vpc', 'describeSecurityGroupPolicies', region]);
        
        if (!describeInstances ||
            !describeSecurityGroups ||
            !describeSecurityGroupPolicies) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (describeSecurityGroups.err || !describeSecurityGroups.data) {
            helpers.addResult(results, 3, `Unable to query for security groups: ${helpers.addError(describeSecurityGroups)}`, region);
            return callback(null, results, source);
        }

        if (describeSecurityGroupPolicies.err || !describeSecurityGroupPolicies.data) {
            helpers.addResult(results, 3, `Unable to query for security group policies: ${helpers.addError(describeSecurityGroupPolicies)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data || !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        if (!describeSecurityGroups.data || !describeSecurityGroups.data.length) {
            helpers.addResult(results, 0, 'No security groups found', region);
            return callback(null, results, source);
        }

        if (!describeSecurityGroupPolicies.data || !describeSecurityGroupPolicies.data.length) {
            helpers.addResult(results, 0, 'No security group policies found', region);
            return callback(null, results, source);
        }

        // Iterate over each instance
        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            // Check if the instance's associated security group has insecure rules
            let hasInsecureRules = false;
            instance.SecurityGroupIds.forEach((sgId) => {
                // Find the policies for this security group
                let sgPolicies = describeSecurityGroupPolicies.data.find((sgPolicy) => sgPolicy.SecurityGroupId == sgId);

                if (sgPolicies) {
                    // Insecure if there's any policy allowing all (0.0.0.0/0)
                    sgPolicies.Ingress.forEach((policy) => {
                        if (policy.CidrBlock == '0.0.0.0/0' && policy.Action == 'ACCEPT' && policy.Port == 'ALL') {
                            hasInsecureRules = true;
                        }
                    });
                }
            });

        
            if (hasInsecureRules) {
                helpers.addResult(results, 2, 'CVM instance has insecure security group rules', region, instance.InstanceId);
            } else {
                helpers.addResult(results, 0, 'CVM instance is not exposed publicly, has secure security group rules and uses encrypted COS bucket', region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};