var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance Public Exposure',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that CVM instances are not exposed publicly.',
    more_info: 'To reduce the potential attack surface, your Tencent CVM instances should not have public IP addresses.',
    recommended_action: 'Modify CVM instances to remove public IP addresses.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);

        if (!describeInstances) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data|| !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (instance.InternetAccessible && instance.InternetAccessible.PublicIpAssigned) {
                helpers.addResult(results, 2, 'CVM instance is exposed publicly', region, instance.InstanceId);
            } else {
                helpers.addResult(results, 0, 'CVM instance is not exposed publicly', region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
