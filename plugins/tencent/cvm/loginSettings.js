var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance Plaintext Password',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that CVM instances do not transmit plaintext passwords.',
    more_info: 'CVM instances should not include a plaintext password in the API response. This presents a serious security risk.',
    recommended_action: 'Remove the plaintext password from the CVM instance login settings.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);

        if (!describeInstances) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data || !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (instance.LoginSettings && instance.LoginSettings.Password) {
                helpers.addResult(results, 2, 'CVM instance is transmitting a plaintext password', region, instance.InstanceId);
            } else {
                helpers.addResult(results, 0, 'CVM instance is not transmitting a plaintext password', region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
