var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'CVM Instance RestrictState',
    category: 'CVM',
    domain: 'Compute',
    description: 'Ensure that CVM instances have their RestrictState set to the expected value.',
    more_info: 'The RestrictState field indicates the instance\'s status related to risk management. It should be set to the expected value.',
    recommended_action: 'Check the RestrictState of CVM instances and set it to the expected value if necessary.',
    link: 'https://intl.cloud.tencent.com/document/product/213/5731',
    apis: ['CVM:describeInstances'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let expectedRestrictState = 'NORMAL'; // replace this with your actual expected state

        let describeInstances = helpers.addSource(cache, source, ['cvm', 'describeInstances', region]);

        if (!describeInstances) return callback(null, results, source);

        if (describeInstances.err || !describeInstances.data) {
            helpers.addResult(results, 3, `Unable to query for CVM instances: ${helpers.addError(describeInstances)}`, region);
            return callback(null, results, source);
        }

        if (!describeInstances.data || !describeInstances.data.length) {
            helpers.addResult(results, 0, 'No CVM instances found', region);
            return callback(null, results, source);
        }

        async.each(describeInstances.data, (instance, cb) => {
            if (!instance.InstanceId) return cb();

            if (instance.RestrictState && instance.RestrictState !== expectedRestrictState) {
                helpers.addResult(results, 2, `CVM instance's RestrictState is not set to the expected value: ${expectedRestrictState}`, region, instance.InstanceId);
            } else {
                helpers.addResult(results, 0, `CVM instance's RestrictState is set to the expected value: ${expectedRestrictState}`, region, instance.InstanceId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
