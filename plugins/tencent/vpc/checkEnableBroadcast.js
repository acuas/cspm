var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Check for EnableBroadcast Setting',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that network broadcast is disabled in all subnets.',
    more_info: 'Enabling network broadcast can introduce security risks and facilitate Denial of Service (DoS) attacks.',
    recommended_action: 'Modify the subnet settings to disable network broadcast (EnableBroadcast: false).',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeSubnets'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeSubnets = helpers.addSource(cache, source, ['vpc', 'describeSubnets', region]);

        if (!describeSubnets) return callback(null, results, source);

        if (describeSubnets.err || !describeSubnets.data) {
            helpers.addResult(results, 3, `Unable to query for Subnets: ${helpers.addError(describeSubnets)}`, region);
            return callback(null, results, source);
        }

        if (!describeSubnets.data || !describeSubnets.data.length) {
            helpers.addResult(results, 0, 'No Subnets found', region);
            return callback(null, results, source);
        }

        async.each(describeSubnets.data, (subnet, cb) => {
            if (!subnet.SubnetId) return cb();

            if (subnet.EnableBroadcast) {
                helpers.addResult(results, 2, 'Network broadcast is enabled in the subnet', region, subnet.SubnetId);
            } else {
                helpers.addResult(results, 0, 'Network broadcast is disabled in the subnet', region, subnet.SubnetId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
