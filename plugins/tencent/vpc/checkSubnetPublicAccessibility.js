var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Check Subnet Public Accessibility',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that subnets are not publicly accessible by checking the associated route table for routes to an Internet Gateway.',
    more_info: 'Subnets with routes to an Internet Gateway are typically publicly accessible, which can introduce security risks.',
    recommended_action: 'Review the associated route table and remove any routes to an Internet Gateway if not required.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs', 'VPC:describeSubnets', 'VPC:describeRouteTables'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeSubnets = helpers.addSource(cache, source, ['vpc', 'describeSubnets', region]);
        let describeRouteTables = helpers.addSource(cache, source, ['vpc', 'describeRouteTables', region]);

        if (!describeSubnets || !describeRouteTables) return callback(null, results, source);

        if (describeSubnets.err || !describeSubnets.data) {
            helpers.addResult(results, 3, `Unable to query for Subnets: ${helpers.addError(describeSubnets)}`, region);
            return callback(null, results, source);
        }

        if (describeRouteTables.err || !describeRouteTables.data) {
            helpers.addResult(results, 3, `Unable to query for Route Tables: ${helpers.addError(describeRouteTables)}`, region);
            return callback(null, results, source);
        }

        if (!describeSubnets.data || !describeSubnets.data.length) {
            helpers.addResult(results, 0, 'No Subnets found', region);
            return callback(null, results, source);
        }

        if (!describeRouteTables.data || !describeRouteTables.data.length) {
            helpers.addResult(results, 0, 'No Route Tables found', region);
            return callback(null, results, source);
        }

        // Create a dictionary of RouteTableId to InternetGateway route presence
        var routeTableInternetGatewayMap = {};
        describeRouteTables.data.forEach(routeTable => {
            let hasInternetGatewayRoute = routeTable.RouteSet && routeTable.RouteSet.some(route => route.GatewayType === 'EIP');
            routeTableInternetGatewayMap[routeTable.RouteTableId] = hasInternetGatewayRoute;
        });

        async.each(describeSubnets.data, (subnet, cb) => {
            if (!subnet.SubnetId || !subnet.RouteTableId) return cb();

            if (routeTableInternetGatewayMap[subnet.RouteTableId]) {
                helpers.addResult(results, 2, 'Subnet is publicly accessible', region, subnet.SubnetId);
            } else {
                helpers.addResult(results, 0, 'Subnet is not publicly accessible', region, subnet.SubnetId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
