var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'VPC DHCP Status',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that DHCP is enabled for all VPCs.',
    more_info: 'DHCP should be enabled for ease of IP management, although this may also introduce additional security risks if not managed properly.',
    recommended_action: 'Modify VPC settings to enable DHCP.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeVpcs = helpers.addSource(cache, source, ['vpc', 'describeVpcs', region]);

        if (!describeVpcs) return callback(null, results, source);

        if (describeVpcs.err || !describeVpcs.data) {
            helpers.addResult(results, 3, `Unable to query for VPCs: ${helpers.addError(describeVpcs)}`, region);
            return callback(null, results, source);
        }

        if (!describeVpcs.data || !describeVpcs.data.length) {
            helpers.addResult(results, 0, 'No VPCs found', region);
            return callback(null, results, source);
        }

        async.each(describeVpcs.data, (vpc, cb) => {
            if (!vpc.VpcId) return cb();

            if (vpc.EnableDhcp) {
                helpers.addResult(results, 0, 'DHCP is enabled for the VPC', region, vpc.VpcId);
            } else {
                helpers.addResult(results, 2, 'DHCP is not enabled for the VPC', region, vpc.VpcId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
