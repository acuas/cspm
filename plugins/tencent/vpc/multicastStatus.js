var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'VPC Multicast Status',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that multicast is disabled for all VPCs unless needed.',
    more_info: 'Multicast should typically be disabled to limit unnecessary traffic unless specifically required for business needs.',
    recommended_action: 'Modify VPC settings to disable multicast.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeVpcs = helpers.addSource(cache, source, ['vpc', 'describeVpcs', region]);

        if (!describeVpcs) return callback(null, results, source);

        if (describeVpcs.err || !describeVpcs.data) {
            helpers.addResult(results, 3, `Unable to query for VPCs: ${helpers.addError(describeVpcs)}`, region);
            return callback(null, results, source);
        }

        if (!describeVpcs.data || !describeVpcs.data.length) {
            helpers.addResult(results, 0, 'No VPCs found', region);
            return callback(null, results, source);
        }

        async.each(describeVpcs.data, (vpc, cb) => {
            if (!vpc.VpcId) return cb();

            if (!vpc.EnableMulticast) {
                helpers.addResult(results, 0, 'Multicast is disabled for the VPC', region, vpc.VpcId);
            } else {
                helpers.addResult(results, 2, 'Multicast is enabled for the VPC', region, vpc.VpcId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
