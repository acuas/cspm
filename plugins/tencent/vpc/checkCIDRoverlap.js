var async = require('async');
var helpers = require('../../../helpers/tencent');
var ip = require('ip');

module.exports = {
    title: 'Assistant CIDR Block Check',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that secondary CIDR blocks do not overlap with the primary CIDR block.',
    more_info: 'Overlapping CIDR blocks can cause IP address conflicts and network routing issues.',
    recommended_action: 'Modify the secondary CIDR blocks to avoid overlap with the primary block.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeVpcs = helpers.addSource(cache, source, ['vpc', 'describeVpcs', region]);

        if (!describeVpcs) return callback(null, results, source);

        if (describeVpcs.err || !describeVpcs.data) {
            helpers.addResult(results, 3, `Unable to query for VPCs: ${helpers.addError(describeVpcs)}`, region);
            return callback(null, results, source);
        }

        if (!describeVpcs.data || !describeVpcs.data.length) {
            helpers.addResult(results, 0, 'No VPCs found', region);
            return callback(null, results, source);
        }

        async.each(describeVpcs.data, (vpc, cb) => {
            if (!vpc.VpcId) return cb();

            let overlap = false;
            let cidrBlockRange = getIPRange(vpc.CidrBlock);

            for (let assistantCidr of vpc.AssistantCidrSet) {
                let assistantCidrRange = getIPRange(assistantCidr);
                let assistantIpv6CidrRange = getIPv6Range(assistantCidr);

                if (isOverlap(cidrBlockRange, assistantCidrRange)) {
                    overlap = true;
                    break;
                }
            }

            if (!overlap) {
                helpers.addResult(results, 0, 'No overlap found between the primary and secondary CIDR blocks', region, vpc.VpcId);
            } else {
                helpers.addResult(results, 2, 'Overlap found between the primary and secondary CIDR blocks', region, vpc.VpcId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });

        function getIPRange(cidrBlock) {
            let range = {};
            if(cidrBlock) {
                range.startIP = ip.toLong(ip.cidrSubnet(cidrBlock).firstAddress);
                range.endIP = ip.toLong(ip.cidrSubnet(cidrBlock).lastAddress);
            }
            return range;
        }


        function isOverlap(range1, range2) {
            if (range1.startIP && range1.endIP && range2.startIP && range2.endIP) {
                return (range1.startIP <= range2.endIP && range1.endIP >= range2.startIP);
            } else {
                return false;
            }
        }
    }
};
