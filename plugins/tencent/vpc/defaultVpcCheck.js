var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Default VPC Check',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that the default VPC is not being used.',
    more_info: 'For security, it is often recommended to delete the default VPC and create new custom VPCs with stricter security controls.',
    recommended_action: 'Modify or delete the default VPC and create new custom VPCs.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeVpcs = helpers.addSource(cache, source, ['vpc', 'describeVpcs', region]);

        if (!describeVpcs) return callback(null, results, source);

        if (describeVpcs.err || !describeVpcs.data) {
            helpers.addResult(results, 3, `Unable to query for VPCs: ${helpers.addError(describeVpcs)}`, region);
            return callback(null, results, source);
        }

        if (!describeVpcs.data || !describeVpcs.data.length) {
            helpers.addResult(results, 0, 'No VPCs found', region);
            return callback(null, results, source);
        }

        async.each(describeVpcs.data, (vpc, cb) => {
            if (!vpc.VpcId) return cb();

            if (!vpc.IsDefault) {
                helpers.addResult(results, 0, 'VPC is not the default', region, vpc.VpcId);
            } else {
                helpers.addResult(results, 2, 'Default VPC is being used', region, vpc.VpcId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
