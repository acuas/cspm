var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Check for Unused Subnets',
    category: 'VPC',
    domain: 'Network',
    description: 'Ensure that all subnets are actively being used and are properly managed.',
    more_info: 'Unused subnets could be a security risk as they might be forgotten and not properly managed.',
    recommended_action: 'Review the utilization of the subnet and remove if not required.',
    link: 'https://intl.cloud.tencent.com/document/product/215/20046',
    apis: ['VPC:describeVpcs','VPC:describeSubnets'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let region = 'eu-frankfurt';

        let describeSubnets = helpers.addSource(cache, source, ['vpc', 'describeSubnets', region]);

        if (!describeSubnets) return callback(null, results, source);

        if (describeSubnets.err || !describeSubnets.data) {
            helpers.addResult(results, 3, `Unable to query for Subnets: ${helpers.addError(describeSubnets)}`, region);
            return callback(null, results, source);
        }

        if (!describeSubnets.data || !describeSubnets.data.length) {
            helpers.addResult(results, 0, 'No Subnets found', region);
            return callback(null, results, source);
        }

        async.each(describeSubnets.data, (subnet, cb) => {
            if (!subnet.SubnetId) return cb();

            if (subnet.AvailableIpAddressCount == subnet.TotalIpAddressCount) {
                helpers.addResult(results, 2, 'Subnet is not utilized', region, subnet.SubnetId);
            } else {
                helpers.addResult(results, 0, 'Subnet is actively used', region, subnet.SubnetId);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
