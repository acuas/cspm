var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Vulnerability Check',
    category: 'CWPP',
    domain: 'Cloud Workload Protection Platform',
    description: 'Ensure that all systems are free of vulnerabilities.',
    more_info: 'Checking for vulnerabilities helps identify any security issues that could be exploited by attackers.',
    recommended_action: 'Check for and fix all identified vulnerabilities.',
    link: 'https://cloud.tencent.com/document/api/296/19874',
    apis: ['CWPP:DescribeVuls', 'CWPP:DescribeVulInfo'],
    compliance: {
        hipaa: 'HIPAA requires the identification and remediation of vulnerabilities in systems that contain ePHI.',
        pci: 'PCI requires regular vulnerability scanning and the timely remediation of identified vulnerabilities.'
    },

    run: async function(cache, settings, callback) {
        var results = [];
        var source = {};

        var region = 'eu-frankfurt';

        var describeVuls = helpers.addSource(cache, source, ['cwpp', 'DescribeVuls', region]);

        if (!describeVuls) return callback(null, results, source);

        if (describeVuls.err || !describeVuls.data) {
            helpers.addResult(results, 3, `Unable to query for vulnerabilities: ${helpers.addError(describeVuls)}`, region);
            return callback(null, results, source);
        }

        if (!describeVuls.data.length) {
            helpers.addResult(results, 0, 'No vulnerabilities found', region);
            return callback(null, results, source);
        }

        async.each(describeVuls.data.Vuls, (vul, cb) => {
            if (!vul && !vul.VulId) return cb();

            var describeVulInfo = helpers.addSource(cache, source, ['cwpp', 'DescribeVulInfo', region, vul.VulId]);

            if (!describeVulInfo || describeVulInfo.err || !describeVulInfo.data) {
                helpers.addResult(results, 3,
                    `Unable to get details for vulnerability ${vul.VulId}: ${helpers.addError(describeVulInfo)}`, region);
                return cb();
            }

            if (vul.VulLevel === 'HIGH' && vul.VulStatus !== 'FIXED') {
                helpers.addResult(results, 2, `High level vulnerability found: ${vul.VulName}`, region);
            } else {
                helpers.addResult(results, 0, `Vulnerability status: ${vul.VulStatus}`, region);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
