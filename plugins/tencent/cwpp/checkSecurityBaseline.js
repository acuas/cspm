var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'Baseline Rules',
    category: 'Security',
    domain: 'Compliance',
    description: 'Print the baseline rules.',
    more_info: 'Baseline rules should be analyzed to understand the security compliance of the system.',
    link: 'https://www.tencentcloud.com/document/product/436/38460',
    apis: ['CWPP:describeBaselineList'],

    run: async function(cache, settings, callback) {
        var results = [];
        var source = {};

        var regions = helpers.regions;
        var region = 'eu-frankfurt';

        var getBaselineRules = helpers.addSource(cache, source, ['cwpp', 'describeBaselineRule', region]);

        if (!getBaselineRules) return callback(null, results, source);

        if (getBaselineRules.err || !getBaselineRules.data) {
            helpers.addResult(results, 3, `Unable to query for baseline rules: ${helpers.addError(getBaselineRules)}`, region);
            return callback(null, results, source);
        }

        if (!getBaselineRules.data.length) {
            helpers.addResult(results, 0, 'No baseline rules found', region);
            return callback(null, results, source);
        }

        async.each(getBaselineRules.data, (rule, cb) => {
            if (!rule.RuleName) return cb();

            var result = {
                ruleName: rule.RuleName,
                description: rule.Description,
                fixMessage: rule.FixMessage,
                level: rule.Level,
                lastScanAt: rule.LastScanAt,
                ruleRemark: rule.RuleRemark
            };

            helpers.addResult(results, 2, rule.Description, region);

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};