var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'User MFA Configuration',
    category: 'CAM',
    domain: 'Identity and Access Management',
    description: 'Ensure that all users have MFA configured.',
    more_info: 'Enabling MFA provides an extra level of security on top of user credentials, helping to protect Tencent Cloud resources.',
    recommended_action: 'Enable MFA for all users.',
    link: 'https://intl.cloud.tencent.com/document/product/598/13674',
    apis: ['CAM:listUsers', 'CAM:describeSafeAuthFlagColl'],
    compliance: {
        hipaa: 'HIPAA requires strong access controls to all user accounts. ' +
                'MFA helps to meet this requirement by providing a second form of authentication.',
        pci: 'PCI requires that MFA is enabled for all non-console access. ' +
             'This applies to all users who can access cardholder data.'
    },

    run: async function(cache, settings, callback) {
        var results = [];
        var source = {};

        var region = 'eu-frankfurt';

        var listUsers = helpers.addSource(cache, source, ['cam', 'listUsers', region]);

        if (!listUsers) return callback(null, results, source);

        if (listUsers.err || !listUsers.data) {
            helpers.addResult(results, 3, `Unable to query for users: ${helpers.addError(listUsers)}`, region);
            return callback(null, results, source);
        }

        if (!listUsers.data.length) {
            helpers.addResult(results, 0, 'No users found', region);
            return callback(null, results, source);
        }

        var describeSafeAuthFlagColl = helpers.addSource(cache, source, ['cam', 'describeSafeAuthFlagColl', region]);

        if (!describeSafeAuthFlagColl || describeSafeAuthFlagColl.err || !describeSafeAuthFlagColl.data) {
            helpers.addResult(results, 3,
                `Unable to query user security settings: ${helpers.addError(describeSafeAuthFlagColl)}`, region, user.Name);
            return cb();
        }

        let listUsersMap = {};
        listUsers.data.forEach((user) => {
            let tmpObj = {
                'user': user
            };
            listUsersMap[user.Uin] = tmpObj;
        });
        describeSafeAuthFlagColl.data.forEach((user) => {
            listUsersMap[user.Uin].describeSafeAuthFlagColl = user;
        });

        async.each(listUsersMap, (user, cb) => {
            if (!user && !user.user && !user.user.Uin) return cb();

            if (user.describeSafeAuthFlagColl.LoginFlag &&
                user.describeSafeAuthFlagColl.LoginFlag.Token === 1) {
                helpers.addResult(results, 0, 'User has MFA enabled', region, user.Name);
            } else {
                helpers.addResult(results, 2, 'User does not have MFA enabled', region, user.Name);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
