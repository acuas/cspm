var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'COS Bucket Lifecycle Configuration',
    category: 'COS',
    domain: 'Storage',
    description: 'Ensures that COS buckets have lifecycle configuration enabled to automatically transition bucket objects.',
    more_info: 'Enabling lifecycle policies for COS buckets enables automatic transition of data from one storage class to another.',
    recommended_action: 'Modify COS buckets to enable lifecycle policies.',
    link: 'https://intl.cloud.tencent.com/document/product/436/8280',
    apis: ['COS:listBuckets', 'COS:getBucketLifecycle'],

    run: function(cache, settings, callback) {
        var results = [];
        var source = {};

        var regions = helpers.regions;
        var region = 'eu-frankfurt';

        var listBuckets = helpers.addSource(cache, source, ['cos', 'listBuckets', region]);

        if (!listBuckets) return callback(null, results, source);

        if (listBuckets.err || !listBuckets.data) {
            helpers.addResult(results, 3, `Unable to query for COS buckets: ${helpers.addError(listBuckets)}`, region);
            return callback(null, results, source);
        }

        if (!listBuckets.data.length) {
            helpers.addResult(results, 0, 'No COS buckets found', region);
            return callback(null, results, source);
        }

        async.each(listBuckets.data, (bucket, cb) => {
            if (!bucket.Name) return cb();

            var getBucketLifecycle = helpers.addSource(cache, source,
                ['cos', 'getBucketLifecycle', region, bucket.Name]);

            var bucketLocation = bucket.Location || region;

            if (bucketLocation !== region && !regions.all.includes(bucketLocation)) return cb();

            // TODO: Add ARN here
            // let resource = helpers.createArn('cos', 'aurasID', 'bucket', bucket.Name, bucketLocation);

            if (!getBucketLifecycle || !getBucketLifecycle.data || getBucketLifecycle.err) {
                helpers.addResult(results, 3,
                    `Unable to get COS bucket lifecycle policy info: ${helpers.addError(getBucketLifecycle)}`, bucketLocation, bucket.Name);
                return cb();
            }

            if (getBucketLifecycle.data.Rules && getBucketLifecycle.data.Rules.length){
                let bucketPolicyEnabled = getBucketLifecycle.data.Rules.find(rule => rule.Status && rule.Status.toUpperCase() == 'ENABLED');

                if (bucketPolicyEnabled){
                    helpers.addResult(results, 0,
                        'COS bucket has lifecycle policy enabled', bucketLocation, bucket.Name);
                } else {
                    helpers.addResult(results, 2,
                        'COS bucket does not have lifecycle policy enabled', bucketLocation, bucket.Name);
                }
            } else {
                helpers.addResult(results, 2,
                    'No lifecycle policy exists', bucketLocation, bucket.Name);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
