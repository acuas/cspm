var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'COS Bucket Encryption',
    category: 'COS',
    domain: 'Storage',
    description: 'Ensure that COS buckets are encrypted.',
    more_info: 'COS buckets should be encrypted to ensure the security and confidentiality of the data they store.',
    recommended_action: 'Modify COS bucket settings to enable encryption.',
    link: 'https://www.tencentcloud.com/document/product/436/38460',
    apis: ['COS:listBuckets', 'COS:getBucketEncryption'],
    compliance: {
        hipaa: 'All data in HIPAA environments must be encrypted, including ' +
                'data at rest. COS encryption ensures that this HIPAA control ' +
                'is implemented by providing KMS-backed encryption for all COS ' +
                'buckets data.',
        pci: 'PCI requires proper encryption of cardholder data at rest. COS ' +
             'encryption should be enabled for all buckets storing this type ' +
             'of data.'
    },

    run: async function(cache, settings, callback) {
        var results = [];
        var source = {};

        var regions = helpers.regions;
        var region = 'eu-frankfurt';

        var listBuckets = helpers.addSource(cache, source, ['cos', 'listBuckets', region]);

        if (!listBuckets) return callback(null, results, source);

        if (listBuckets.err || !listBuckets.data) {
            helpers.addResult(results, 3, `Unable to query for cos buckets: ${helpers.addError(listBuckets)}`, region);
            return callback(null, results, source);
        }

        if (!listBuckets.data.length) {
            helpers.addResult(results, 0, 'No COS buckets found', region);
            return callback(null, results, source);
        }

        async.each(listBuckets.data, (bucket, cb) => {
            if (!bucket.Name) return cb();

            var getBucketEncryption = helpers.addSource(cache, source,
                ['cos', 'getBucketEncryption', region, bucket.Name]);

            var bucketLocation = bucket.Location || region;

            if (bucketLocation !== region && !regions.all.includes(bucketLocation)) return cb();

            if (!getBucketEncryption || getBucketEncryption.err || !getBucketEncryption.data) {
                helpers.addResult(results, 3,
                    `Unable to query COS bucket info: ${helpers.addError(getBucketEncryption)}`, bucketLocation, bucket.Name);
                return cb();
            }

            if (getBucketEncryption.data.ServerSideEncryptionConfiguration &&
                getBucketEncryption.data.ServerSideEncryptionConfiguration.Rule &&
                getBucketEncryption.data.ServerSideEncryptionConfiguration.Rule.ApplyServerSideEncryptionByDefault &&
                getBucketEncryption.data.ServerSideEncryptionConfiguration.Rule.ApplyServerSideEncryptionByDefault.SSEAlgorithm === 'AES256') {
                helpers.addResult(results, 0, 'Bucket has encryption enabled', bucketLocation, bucket.Name);
            } else {
                helpers.addResult(results, 2, 'Bucket does not have encryption enabled', bucketLocation, bucket.Name);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
