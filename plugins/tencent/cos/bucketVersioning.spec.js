var expect = require('chai').expect;
var cosBucketVersioning = require('./bucketVersioning.js');

const listBuckets = [
    {
        "Name": 'anpopescu-test-1315194554',
        "Location": 'eu-frankfurt',
        "CreationDate": '2022-11-25T19:05:20Z',
        "BucketType": 'cos'
    }
];

const getBucketVersioning = [
    { Status: 'Enabled' },
    { },
];

const createCache = (listBuckets, getBucketVersioning, listBucketsErr, getBucketVersioningErr) => {
    let bucketName = (listBuckets && listBuckets.length) ? listBuckets[0].Name : null;
    return {
        cos: {
            listBuckets: {
                'eu-frankfurt': {
                    data: listBuckets,
                    err: listBucketsErr
                },
            },
            getBucketVersioning: {
                'eu-frankfurt': {
                    [bucketName]: {
                        data: getBucketVersioning,
                        err: getBucketVersioningErr
                    }
                }
            }
        },
    };
};

describe('cosBucketVersioning', function () {
    describe('run', function () {
        it('should FAIL if bucket versioning is not enabled', function (done) {
            const cache = createCache(listBuckets, getBucketVersioning[1]);
            cosBucketVersioning.run(cache, { china: true }, (err, results) => {
                expect(results.length).to.equal(1);
                expect(results[0].status).to.equal(2);
                expect(results[0].message).to.include('Bucket does not have versioning enabled');
                expect(results[0].region).to.equal('eu-frankfurt');
                done();
            });
        });

        it('should PASS if bucket versioning is enabled', function (done) {
            const cache = createCache(listBuckets, getBucketVersioning[0]);
            cosBucketVersioning.run(cache, { china: true }, (err, results) => {
                expect(results.length).to.equal(1);
                expect(results[0].status).to.equal(0);
                expect(results[0].message).to.include('Bucket has versioning enabled');
                expect(results[0].region).to.equal('eu-frankfurt');
                done();
            });
        });

        it('should PASS if no COS buckets found', function (done) {
            const cache = createCache([]);
            cosBucketVersioning.run(cache, { china: true }, (err, results) => {
                expect(results.length).to.equal(1);
                expect(results[0].status).to.equal(0);
                expect(results[0].message).to.include('No COS buckets found');
                expect(results[0].region).to.equal('eu-frankfurt');
                done();
            });
        });
        //
        // it('should UNKNOWN if unable to query for OSS buckets', function (done) {
        //     const cache = createCache([], null, { err: 'Unable to query for OSS buckets' });
        //     cosBucketVersioning.run(cache, { china: true }, (err, results) => {
        //         expect(results.length).to.equal(1);
        //         expect(results[0].status).to.equal(3);
        //         expect(results[0].message).to.include('Unable to query for OSS buckets');
        //         expect(results[0].region).to.equal('cn-hangzhou');
        //         done();
        //     });
        // });
        //
        // it('should UNKNOWN if unable to query OSS bucket info', function (done) {
        //     const cache = createCache(listBuckets, {}, null, { err: 'Unable to query OSS bucket info' });
        //     cosBucketVersioning.run(cache, { china: true }, (err, results) => {
        //         expect(results.length).to.equal(1);
        //         expect(results[0].status).to.equal(3);
        //         expect(results[0].message).to.include('Unable to query OSS bucket info');
        //         expect(results[0].region).to.equal('cn-hangzhou');
        //         done();
        //     });
        // });
    })
})