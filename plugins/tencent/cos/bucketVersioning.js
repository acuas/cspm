var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'COS Bucket Versioning',
    category: 'COS',
    domain: 'Storage',
    description: 'Ensure that COS bucket has versioning enabled.',
    more_info: 'COS allows you to configure versioning to protect data in buckets. When versioning is enabled for a bucket, ' +
        'data that is overwritten or deleted in the bucket is saved as a previous version.',
    recommended_action: 'Modify bucket settings to enabled versioning.',
    link: 'https://www.tencentcloud.com/document/product/436/19881',
    apis: ['COS:listBuckets', 'COS:getBucketVersioning'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let regions = helpers.regions;
        let region = 'eu-frankfurt';
        //
        // let accountId = helpers.addSource(cache, source, ['sts', 'GetCallerIdentity', region, 'data']);

        let listBuckets = helpers.addSource(cache, source, ['cos', 'listBuckets', region]);

        if (!listBuckets) return callback(null, results, source);

        if (listBuckets.err || !listBuckets.data) {
            helpers.addResult(results, 3, `Unable to query for cos buckets: ${helpers.addError(listBuckets)}`, region);
            return callback(null, results, source);
        }

        if (!listBuckets.data.length) {
            helpers.addResult(results, 0, 'No COS buckets found', region);
            return callback(null, results, source);
        }

        async.each(listBuckets.data, (bucket, cb) => {
            if (!bucket.Name) return cb();

            let getBucketVersioning = helpers.addSource(cache, source,
                ['cos', 'getBucketVersioning', region, bucket.Name]);

            let bucketLocation = bucket.Location || region;

            if (bucketLocation !== region && !regions.all.includes(bucketLocation)) return cb();

            // let resource = helpers.createArn('cos', 'aurasID', 'bucket', bucket.Name, bucketLocation);
            // TODO: Add ARN here

            if (!getBucketVersioning || getBucketVersioning.err || !getBucketVersioning.data) {
                helpers.addResult(results, 3,
                    `Unable to query COS bucket info: ${helpers.addError(getBucketVersioning)}`, bucketLocation, bucket.Name);
                return cb();
            }

            if (getBucketVersioning.data.Status && getBucketVersioning.data.Status.toUpperCase() === 'ENABLED') {
                helpers.addResult(results, 0, 'Bucket has versioning enabled', bucketLocation, bucket.Name);
            } else {
                helpers.addResult(results, 2, 'Bucket does not have versioning enabled', bucketLocation, bucket.Name);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
