var async = require('async');
var helpers = require('../../../helpers/tencent');

module.exports = {
    title: 'COS Bucket ACL',
    category: 'COS',
    domain: 'Storage',
    description: 'Ensure that COS bucket does not allow global write, delete, or read ACL permissions',
    more_info: 'COS buckets can be configured to allow anyone, regardless of whether they are an Tencent user or not, to write objects to a bucket or delete objects. This option should not be configured unless there is a strong business requirement.',
    recommended_action: 'Modify bucket ACL settings to disallow global all users policies.',
    link: 'https://intl.cloud.tencent.com/document/product/436/32304',
    apis: ['COS:listBuckets', 'COS:getBucketAcl'],

    run: async function(cache, settings, callback) {
        let results = [];
        let source = {};

        let regions = helpers.regions;
        let region = 'eu-frankfurt';

        let listBuckets = helpers.addSource(cache, source, ['cos', 'listBuckets', region]);

        if (!listBuckets) return callback(null, results, source);

        if (listBuckets.err || !listBuckets.data) {
            helpers.addResult(results, 3, `Unable to query for cos buckets: ${helpers.addError(listBuckets)}`, region);
            return callback(null, results, source);
        }

        if (!listBuckets.data.length) {
            helpers.addResult(results, 0, 'No COS buckets found', region);
            return callback(null, results, source);
        }

        async.each(listBuckets.data, (bucket, cb) => {
            if (!bucket.Name) return cb();

            let getBucketAcl = helpers.addSource(cache, source,
                ['cos', 'getBucketAcl', region, bucket.Name]);

            let bucketLocation = bucket.Location || region;

            if (bucketLocation !== region && !regions.all.includes(bucketLocation)) return cb();

            // TODO: Add ARN here

            if (!getBucketAcl || getBucketAcl.err || !getBucketAcl.data) {
                helpers.addResult(results, 3,
                    `Unable to query COS bucket ACL: ${helpers.addError(getBucketAcl)}`, bucketLocation, bucket.Name);
                return cb();
            }

            if (getBucketAcl.data.ACL && getBucketAcl.data.ACL.toLowerCase() !== 'private') {
                helpers.addResult(results, 2, 'Bucket allows public access', bucketLocation, bucket.Name);
            } else {
                helpers.addResult(results, 0, 'Bucket does not allow public access', bucketLocation, bucket.Name);
            }

            cb();
        }, function() {
            callback(null, results, source);
        });
    }
};
