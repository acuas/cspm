// cspm config file

module.exports = {
    credentials: {
        tencent: {
            secret_id: 'SecretID',
            secret_key: 'SecretKey',
            region: 'eu-frankfurt'
        },
        alibaba: {
            access_key_id: 'KeyID',
            access_key_secret: 'KeySecret'
        }
    }
};