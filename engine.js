var async = require('async');
var exports = require('./exports.js');
var suppress = require('./postprocess/suppress.js');
var output = require('./postprocess/output.js');

/**
 * The main function to execute CSPM scans.
 * @param cloudConfig The configuration for the cloud provider.
 * @param settings General purpose settings.
 */
var engine = function(cloudConfig, settings) {
    // Initialize any suppression rules based on the the command line arguments
    var suppressionFilter = suppress.create(settings.suppress);

    // Initialize the output handler
    var outputHandler = output.create(settings);

    // Configure Service Provider Collector
    var collector = require(`./collectors/${settings.cloud}/collector.js`);
    var plugins = exports[settings.cloud];
    var apiCalls = [];

    // Load resource mappings
    var resourceMap;
    try {
        resourceMap = require(`./helpers/${settings.cloud}/resources.js`);
    } catch (e) {
        resourceMap = {};
    }

    // Print customization options
    if (settings.compliance) console.log(`INFO: Using compliance modes: ${settings.compliance.join(', ')}`);
    if (settings.ignore_ok) console.log('INFO: Ignoring passing results');
    if (settings.suppress && settings.suppress.length) console.log('INFO: Suppressing results based on suppress flags');
    if (settings.plugin) {
        if (!plugins[settings.plugin]) return console.log(`ERROR: Invalid plugin: ${settings.plugin}`);
        console.log(`INFO: Testing plugin: ${plugins[settings.plugin].title}`);
    }

    // STEP 1 - Obtain API calls to make
    console.log('INFO: Determining API calls to make...');

    var skippedPlugins = [];

    Object.entries(plugins).forEach(function(p){
        var pluginId = p[0];
        var plugin = p[1];

        // Skip plugins that don't match the ID flag
        var skip = false;
        if (settings.plugin && settings.plugin !== pluginId) {
            skip = true;
        } else {
            if (settings.compliance && settings.compliance.length) {
                if (!plugin.compliance || !Object.keys(plugin.compliance).length) {
                    skip = true;
                    console.debug(`DEBUG: Skipping plugin ${plugin.title} because it is not used for compliance programs`);
                } else {
                    // Compare
                    var cMatch = false;
                    settings.compliance.forEach(function(c){
                        if (plugin.compliance[c]) cMatch = true;
                    });
                    if (!cMatch) {
                        skip = true;
                        console.debug(`DEBUG: Skipping plugin ${plugin.title} because it did not match compliance programs ${settings.compliance.join(', ')}`);
                    }
                }
            }
        }

        if (skip) {
            skippedPlugins.push(pluginId);
        } else {
            plugin.apis.forEach(function(api) {
                if (apiCalls.indexOf(api) === -1) apiCalls.push(api);
            });
        }
    });

    if (!apiCalls.length) return console.log('ERROR: Nothing to collect.');

    console.log(`INFO: Found ${apiCalls.length} API calls to make for ${settings.cloud} plugins`);
    console.log('INFO: Collecting metadata. This may take several minutes...');

    // STEP 2 - Collect API Metadata from Service Providers
    collector(cloudConfig, {
        api_calls: apiCalls,
        paginate: settings.skip_paginate,
    }, function(err, collection) {
        if (err || !collection || !Object.keys(collection).length) return console.log(`ERROR: Unable to obtain API metadata: ${err || 'No data returned'}`);
        outputHandler.writeCollection(collection, settings.cloud);

        console.log('INFO: Metadata collection complete. Analyzing...');
        console.log('INFO: Analysis complete. Scan report to follow...');

        var maximumStatus = 0;
        
        function executePlugins(cloudRemediateConfig) {
            async.mapValuesLimit(plugins, 10, function(plugin, key, pluginDone) {
                if (skippedPlugins.indexOf(key) > -1) return pluginDone(null, 0);

                var postRun = function(err, results) {
                    if (err) return console.log(`ERROR: ${err}`);
                    if (!results || !results.length) {
                        console.log(`Plugin ${plugin.title} returned no results. There may be a problem with this plugin.`);
                    } else {
                        for (var r in results) {
                            // If we have suppressed this result, then don't process it
                            // so that it doesn't affect the return code.
                            if (suppressionFilter([key, results[r].region || 'any', results[r].resource || 'any'].join(':'))) {
                                continue;
                            }

                            var complianceMsg = [];
                            if (settings.compliance && settings.compliance.length) {
                                settings.compliance.forEach(function(c) {
                                    if (plugin.compliance && plugin.compliance[c]) {
                                        complianceMsg.push(`${c.toUpperCase()}: ${plugin.compliance[c]}`);
                                    }
                                });
                            }
                            complianceMsg = complianceMsg.join('; ');
                            if (!complianceMsg.length) complianceMsg = null;

                            // Write out the result (to console or elsewhere)
                            outputHandler.writeResult(results[r], plugin, key, complianceMsg);

                            // Add this to our tracking for the worst status to calculate
                            // the exit code
                            maximumStatus = Math.max(maximumStatus, results[r].status);
                        }
                    }
                    setTimeout(function() { pluginDone(err, maximumStatus); }, 0);
                };

                plugin.run(collection, settings, postRun);
            }, function(err) {
                if (err) return console.log(err);

                outputHandler.close();
                if (settings.exit_code) {
                    console.log(`INFO: Exiting with exit code: ${maximumStatus}`);
                    process.exitCode = maximumStatus;
                }
                console.log('INFO: Scan complete');
            });
        }

        executePlugins(cloudConfig);
    });
};

module.exports = engine;
