const async = require('async');

let helpers = require(__dirname + '/../../helpers/tencent');
let collectors = require(__dirname + '/../../collectors/tencent');

let calls = {
    MARIADB: {
        describeDBInstances: {
            override: true
        },
    },
    COS: {
        listBuckets: {
            override: true
        },
    },
    CVM: {
        describeInstances: {
            override: true
        },
    },
    VPC: {
        describeVpcs: {
            override: true
        },
        describeSecurityGroups: {
            override: true
        },
    },
    CAM: {
        listUsers: {
            override: true
        }
    },
    CWPP: {
        describeVuls: {
            override: true
        },
        describeBaselineList: {
            override: true
        }
    }
};

let postcalls = [
    {
        COS: {
            getBucketVersioning: {
                reliesOnService: 'cos',
                reliesOnCall: 'listBuckets',
                override: true
            },
            getBucketAcl: {
                reliesOnService: 'cos',
                reliesOnCall: 'listBuckets',
                override: true
            },
            getBucketEncryption: {
                reliesOnService: 'cos',
                reliesOnCall: 'listBuckets',
                override: true
            },
            getBucketLifecycle: {
                reliesOnService: 'cos',
                reliesOnCall: 'listBuckets',
                override: true
            }
        },
        VPC: {
            describeSubnets: {
                reliesOnService: 'vpc',
                reliesOnCall: 'describeVpcs',
                override: true
            },  
            describeRouteTables: {
                reliesOnService: 'vpc',
                reliesOnCall: 'describeVpcs',
                override: true
            },
            describeNetworkAcls: {
                reliesOnService: 'vpc',
                reliesOnCall: 'describeVpcs',
                override: true
            },
            describeSecurityGroupPolicies: {
                reliesOnService: 'vpc',
                reliesOnCall: 'describeSecurityGroups',
                override: true
            },
        },
        MARIADB: {
            describeDBInstanceDetail: {
                reliesOnService: 'mariadb',
                reliesOnCall: 'describeDBInstances',
                override: true
            },
            describeDBSecurityGroups: {
                reliesOnService: 'mariadb',
                reliesOnCall: 'describeDBInstances',
                override: true
            },
            describeAccounts: {
                reliesOnService: 'mariadb',
                reliesOnCall: 'describeDBInstances',
                override: true
            }
        },
        CAM: {
            describeSafeAuthFlagColl: {
                reliesOnService: 'cam',
                reliesOnCall: 'listUsers',
                override: true
            }
        },
        CWPP: {
            describeVulInfo: {
                reliesOnService: 'cwpp',
                reliesOnCall: 'describeVuls',
                override: true
            }
        }
    }
];

let collect = function(TencentConfig, settings, callback) {
    if (settings.gather) {
        return callback(null, calls, postcalls);
    }

    var regions = helpers.regions;

    var collection = {};

    async.eachOfLimit(calls, 10, function(call, service, serviceCb) {
        let serviceLower = service.toLowerCase();
        if (!collection[serviceLower]) collection[serviceLower] = {};

        async.eachOfLimit(call, 15, function(callObj, callKey, callCb) {
            if (!collection[serviceLower][callKey]) collection[serviceLower][callKey] = {};

            let callRegions = regions[serviceLower];
            async.eachLimit(callRegions, helpers.MAX_REGIONS_AT_A_TIME, function(region, regionCb) {
                if (!collection[serviceLower][callKey][region]) collection[serviceLower][callKey][region] = {};

                let LocalTencentConfig = JSON.parse(JSON.stringify(TencentConfig));

                if (callObj.override) {
                    collectors[serviceLower][callKey](LocalTencentConfig, collection, region, function() {
                        if (callObj.rateLimit) {
                            setTimeout(function() {
                                regionCb();
                            }, callObj.rateLimit);
                        } else {
                            regionCb();
                        }
                    });
                }
            }, function() {
                callCb();
            });
        }, function() {
            serviceCb();
        });
    }, function() {
        async.eachSeries(postcalls, function(postcallObj, postcallCb) {
            async.eachOfLimit(postcallObj, 10, function(serviceObj, service, serviceCb) {
                let serviceLower = service.toLowerCase();
                if (!collection[serviceLower]) collection[serviceLower] = {};

                async.eachOfLimit(serviceObj, 1, function(callObj, callKey, callCb) {
                    if (!collection[serviceLower][callKey]) collection[serviceLower][callKey] = {};

                    async.eachLimit(regions[serviceLower], helpers.MAX_REGIONS_AT_A_TIME, function(region, regionCb) {

                        if (!collection[serviceLower][callKey][region]) collection[serviceLower][callKey][region] = {};

                        if (callObj.reliesOnService && !collection[callObj.reliesOnService]) return regionCb();

                        if (callObj.reliesOnCall &&
                            (!collection[callObj.reliesOnService] ||
                                !collection[callObj.reliesOnService][callObj.reliesOnCall] ||
                                !collection[callObj.reliesOnService][callObj.reliesOnCall][region] ||
                                !collection[callObj.reliesOnService][callObj.reliesOnCall][region].data ||
                                !collection[callObj.reliesOnService][callObj.reliesOnCall][region].data.length))
                            return regionCb();

                        let LocalTencentConfig = JSON.parse(JSON.stringify(TencentConfig));

                        if (callObj.override) {
                            collectors[serviceLower][callKey](LocalTencentConfig, collection, region, function() {
                                if (callObj.rateLimit) {
                                    setTimeout(function() {
                                        regionCb();
                                    }, callObj.rateLimit);
                                } else {
                                    regionCb();
                                }
                            });
                        }
                    }, function() {
                        callCb();
                    });
                }, function() {
                    serviceCb();
                });
            }, function() {
                postcallCb();
            });
        }, function() {
            callback(null, collection);
        });
    });
};

module.exports = collect;
