const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const VpcClient = tencentcloud.vpc.v20170312.Client;
    const models = tencentcloud.vpc.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new VpcClient(cred, region);

    async.eachLimit(collection.vpc.describeVpcs[region].data, 10, function(vpc, bcb){
        let req = new models.DescribeNetworkAclsRequest();

        // Define filter by VPC ID
        let filter = new models.Filter();

        filter.Name = 'vpc-id';
        filter.Values = [vpc.VpcId]; 
        req.Filters = [filter];

        collection.vpc.describeNetworkAcls[region].data = [];

        let callCB = function(err, data) {
            if (err) {
                collection.vpc.describeNetworkAcls[region].err = err;
                return bcb();
            }
            if (!data || !data.NetworkAclSet || !data.NetworkAclSet.length) {
                return bcb();
            }
            collection.vpc.describeNetworkAcls[region].data = collection.vpc.describeNetworkAcls[region].data.concat(data.NetworkAclSet);
            return bcb();
        };

        let execute = function() {
            client.DescribeNetworkAcls(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
