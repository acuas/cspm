const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const VpcClient = tencentcloud.vpc.v20170312.Client;
    const models = tencentcloud.vpc.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new VpcClient(cred, region);

    async.eachLimit(collection.vpc.describeSecurityGroups[region].data, 10, function(sg, bcb){
        let req = new models.DescribeSecurityGroupPoliciesRequest();
        req.SecurityGroupId = sg.SecurityGroupId;

        collection.vpc.describeSecurityGroupPolicies[region].data = [];

        let callCB = function(err, data) {
            if (err) {
                collection.vpc.describeSecurityGroupPolicies[region].err = err;
                return bcb();
            }
            if (!data || !data.SecurityGroupPolicySet) {
                return bcb();
            }

            data.SecurityGroupPolicySet.SecurityGroupId = req.SecurityGroupId;
            collection.vpc.describeSecurityGroupPolicies[region].data = collection.vpc.describeSecurityGroupPolicies[region].data.concat(data.SecurityGroupPolicySet);

            return bcb();
        };

        let execute = function() {
            client.DescribeSecurityGroupPolicies(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
