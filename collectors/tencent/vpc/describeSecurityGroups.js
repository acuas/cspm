const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const VpcClient = tencentcloud.vpc.v20170312.Client;
    const models = tencentcloud.vpc.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new VpcClient(cred, region);
    let req = new models.DescribeSecurityGroupsRequest();

    collection.vpc.describeSecurityGroups[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.vpc.describeSecurityGroups[region].err = err;
            return callback();
        }
        if (!data || !data.SecurityGroupSet || !data.SecurityGroupSet.length) {
            return callback();
        }
        collection.vpc.describeSecurityGroups[region].data = collection.vpc.describeSecurityGroups[region].data.concat(data.SecurityGroupSet);
        return callback();
    };

    let execute = function() {
        client.DescribeSecurityGroups(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
