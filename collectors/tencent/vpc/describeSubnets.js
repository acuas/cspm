const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const VpcClient = tencentcloud.vpc.v20170312.Client;
    const models = tencentcloud.vpc.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new VpcClient(cred, region);

    async.eachLimit(collection.vpc.describeVpcs[region].data, 10, function(vpc, bcb){
        let req = new models.DescribeSubnetsRequest();

        // Define filter by VPC ID
        let filter = new models.Filter();

        filter.Name = 'vpc-id';
        filter.Values = [vpc.VpcId]; 
        req.Filters = [filter];

        collection.vpc.describeSubnets[region].data = [];

        let callCB = function(err, data) {
            if (err) {
                collection.vpc.describeSubnets[region].err = err;
                return bcb();
            }
            if (!data || !data.SubnetSet || !data.SubnetSet.length) {
                return bcb();
            }
            collection.vpc.describeSubnets[region].data = collection.vpc.describeSubnets[region].data.concat(data.SubnetSet);
            return bcb();
        };

        let execute = function() {
            client.DescribeSubnets(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
