const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const VpcClient = tencentcloud.vpc.v20170312.Client;
    const models = tencentcloud.vpc.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new VpcClient(cred, region);
    let req = new models.DescribeVpcsRequest();

    collection.vpc.describeVpcs[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.vpc.describeVpcs[region].err = err;
            return callback();
        }
        if (!data || !data.VpcSet || !data.VpcSet.length) {
            return callback();
        }
        collection.vpc.describeVpcs[region].data = collection.vpc.describeVpcs[region].data.concat(data.VpcSet);
        return callback();
    };

    let execute = function() {
        client.DescribeVpcs(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
