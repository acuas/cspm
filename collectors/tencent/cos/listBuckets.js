const COS = require('cos-nodejs-sdk-v5');

module.exports = function(TencentConfig, collection, region, callback) {
    let localTencentConfig = JSON.parse(JSON.stringify(TencentConfig));
    localTencentConfig['timeout'] = 300000;
  
    let cos = new COS(localTencentConfig);
    collection.cos.listBuckets[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.cos.listBuckets[region].err = err;
            return callback();
        }
        if (!data || !data.Buckets || !data.Buckets.length) {
            return callback();
        }
        collection.cos.listBuckets[region].data = collection.cos.listBuckets[region].data.concat(data.Buckets);
        return callback();
    };

    let execute = function() {
        cos.getService({
            Region: region
        }, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
