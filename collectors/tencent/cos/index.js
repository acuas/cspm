var async = require('async');
const COS = require('cos-nodejs-sdk-v5');

module.exports = function(callKey, TencentConfig, collection, region, callback) {
    async.eachLimit(collection.cos.listBuckets[region].data, 10, function(bucket, bcb){
        if (!bucket) return bcb();
        let localTencentConfig = JSON.parse(JSON.stringify(TencentConfig));
        if (bucket.Location) localTencentConfig['region'] = bucket.Location;
        let cos = new COS(localTencentConfig);
        let bucketName = bucket.Name;
        collection.cos[callKey][region][bucketName] = {};

        cos[callKey]({
            Bucket: bucketName,
            Region: region
        }).then((result) => {
            if (callKey === 'getBucketVersioning') {
                collection.cos[callKey][region][bucketName].data = result.VersioningConfiguration;
            } else  {
                collection.cos[callKey][region][bucketName].data = result;
            }
            bcb();
        }, (err) => {
            collection.cos[callKey][region][bucketName].err = err;
            bcb();
        });
    }, function(){
        callback();
    });
};