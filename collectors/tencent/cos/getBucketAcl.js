const index = require(__dirname + '/index.js');

module.exports = function(TencentConfig, collection, region, callback) {
    index('getBucketAcl', TencentConfig, collection, region, callback);
};