const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs");

module.exports = function(TencentConfig, collection, region, callback) {
    const CwpClient = tencentcloud.cwp.v20180228.Client;
    const models = tencentcloud.cwp.v20180228.Models;
    const client = new CwpClient({
        credential: {
            secretId: TencentConfig.SecretId,
            secretKey: TencentConfig.SecretKey
        },
        region: region,
        profile: {
            signMethod: "TC3-HMAC-SHA256",
            language: "en-US"
        }
    });

    collection.cwpp.describeBaselineList[region].data = [];
    collection.cwpp.describeBaselineEffectHostList = {};
    collection.cwpp.describeBaselineEffectHostList[region] = {};
    collection.cwpp.describeBaselineEffectHostList[region].data = [];
    collection.cwpp.describeBaselineRule = {};
    collection.cwpp.describeBaselineRule[region] = {};
    collection.cwpp.describeBaselineRule[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.cwpp.describeBaselineList[region].err = err;
            return callback();
        }
        if (!data || !data.BaselineList || !data.BaselineList.length) {
            return callback();
        }
        collection.cwpp.describeBaselineList[region].data = collection.cwpp.describeBaselineList[region].data.concat(data.BaselineList);
        
        // DescribeBaselineEffectHostList
        async.eachLimit(collection.cwpp.describeBaselineList[region].data, 10, function(baseline, callback2) {
            let callCB2 = function(err, data) {
                if (err) {
                    collection.cwpp.describeBaselineEffectHostList[region].err = err;
                    return callback2();
                }
                if (!data || !data.EffectHostList || !data.EffectHostList.length) {
                    return callback2();
                }
                data.EffectHostList.forEach(function(host) {
                    host.CategoryId = baseline.CategoryId;
                });
                collection.cwpp.describeBaselineEffectHostList[region].data = collection.cwpp.describeBaselineEffectHostList[region].data.concat(data.EffectHostList);
                return callback2();
            };
            
            let execute2 = function() {
                client.DescribeBaselineEffectHostList({
                    BaselineId: baseline.CategoryId,
                    Offset: 0,
                    Limit: 100,
                    StrategyId: 1170
                }, function(err, data) {
                    if (err) {
                        return callCB2(err);
                    } else {
                        return callCB2(null, data);
                    }


                });
            };

            execute2();
        }, function(err) {
            if (err) {
                return callback(err);
            }

            async.eachLimit(collection.cwpp.describeBaselineEffectHostList[region].data, 10, function(host, callback3) {
                let callCB3 = function(err, data) {
                    if (err) {
                        collection.cwpp.describeBaselineRule[region].err = err;
                        return callback3();
                    }
                    if (!data || !data.BaselineRuleList || !data.BaselineRuleList.length) {
                        return callback3();
                    }
                    collection.cwpp.describeBaselineRule[region].data = collection.cwpp.describeBaselineRule[region].data.concat(data.BaselineRuleList);
                    return callback3();
                };
                
                let execute3 = function() {
                    client.DescribeBaselineRule({
                        BaselineId: host.CategoryId,
                        Offset: 0,
                        Limit: 100,
                        Quuid: host.Quuid
                    }, function(err, data) {
                        if (err) {
                            return callCB3(err);
                        } else {
                            return callCB3(null, data);
                        }
                    });
                };

                execute3();
            }, function(err) {
                if (err) {
                    return callback(err);
                }
                return callback();
            }); 
        });
    };

    let execute = function() {
        client.DescribeBaselineList({
            Limit: 100,
            Offset: 0,
            Filters: [
                {
                    Name: "StrategyId",
                    Values: ["1170"]
                }
            ]
        }, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
