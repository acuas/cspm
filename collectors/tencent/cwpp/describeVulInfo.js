const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const YunjingClient = tencentcloud.yunjing.v20180228.Client;
    const models = tencentcloud.yunjing.v20180228.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new YunjingClient(cred, region);
    collection.cwpp.describeVulInfo[region].data = [];

    async.eachLimit(collection.cwpp.describeVuls[region].data, 10, function(vul, bcb){
        let req = new models.DescribeVulInfoRequest();
        req.VulId = vul.VulId;

        let callCB = function(err, data) {
            if (err) {
                collection.cwpp.describeVulInfo[region].err = err;
                return bcb();
            }
            if (!data) {
                return bcb();
            }

            collection.cwpp.describeVulInfo[region].data =  collection.cwpp.describeVulInfo[region].data.concat(data);
            return bcb();
        };

        let execute = function() {
            client.DescribeVulInfo(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
