const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const YunjingClient = tencentcloud.yunjing.v20180228.Client;
    const models = tencentcloud.yunjing.v20180228.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new YunjingClient(cred, region);
    let req = new models.DescribeVulsRequest();

    collection.cwpp.describeVuls[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.cwpp.describeVuls[region].err = err;
            return callback();
        }
        if (!data || !data.Vuls || !data.Vuls.length) {
            return callback();
        }
        collection.cwpp.describeVuls[region].data = collection.cwpp.describeVuls[region].data.concat(data.Vuls);
        return callback();
    };

    let execute = function() {
        client.DescribeVuls(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
