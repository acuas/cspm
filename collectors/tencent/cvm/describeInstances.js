const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const CvmClient = tencentcloud.cvm.v20170312.Client;
    const models = tencentcloud.cvm.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new CvmClient(cred, region);
    let req = new models.DescribeInstancesRequest();

    collection.cvm.describeInstances[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.cvm.describeInstances[region].err = err;
            return callback();
        }
        if (!data || !data.InstanceSet || !data.InstanceSet.length) {
            return callback();
        }
        collection.cvm.describeInstances[region].data = collection.cvm.describeInstances[region].data.concat(data.InstanceSet);
        return callback();
    };

    let execute = function() {
        client.DescribeInstances(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
