const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const MariaDBClient = tencentcloud.mariadb.v20170312.Client;
    const models = tencentcloud.mariadb.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new MariaDBClient(cred, region);
    collection.mariadb.describeDBInstanceDetail[region].data = [];

    async.eachLimit(collection.mariadb.describeDBInstances[region].data, 10, function(instance, bcb){
        let req = new models.DescribeDBInstanceDetailRequest();
        req.InstanceId = instance.InstanceId;

        let callCB = function(err, data) {
            if (err) {
                collection.mariadb.describeDBInstanceDetail[region].err = err;
                return bcb();
            }
            if (!data) {
                return bcb();
            }

            data.InstanceId = req.InstanceId;
            collection.mariadb.describeDBInstanceDetail[region].data = collection.mariadb.describeDBInstanceDetail[region].data.concat(data);

            return bcb();
        };

        let execute = function() {
            client.DescribeDBInstanceDetail(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
