const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const MariaDBClient = tencentcloud.mariadb.v20170312.Client;
    const models = tencentcloud.mariadb.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new MariaDBClient(cred, region);
    collection.mariadb.describeDBSecurityGroups[region].data = [];

    async.eachLimit(collection.mariadb.describeDBInstances[region].data, 10, function(db, bcb){
        let req = new models.DescribeDBSecurityGroupsRequest();
        req.InstanceId = db.InstanceId;
        req.Product = "mariadb";

        let callCB = function(err, data) {
            if (err) {
                collection.mariadb.describeDBSecurityGroups[region].err = err;
                return bcb();
            }
            if (!data || !data.Groups) {
                return bcb();
            }

            data.Groups.forEach((group) => {
                group.InstanceId = req.InstanceId;
            });
            
            collection.mariadb.describeDBSecurityGroups[region].data = collection.mariadb.describeDBSecurityGroups[region].data.concat(data.Groups);

            return bcb();
        };

        let execute = function() {
            client.DescribeDBSecurityGroups(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
