const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const MariadbClient = tencentcloud.mariadb.v20170312.Client;
    const models = tencentcloud.mariadb.v20170312.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new MariadbClient(cred, region);
    let req = new models.DescribeDBInstancesRequest();

    collection.mariadb.describeDBInstances[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.mariadb.describeDBInstances[region].err = err;
            return callback();
        }
        if (!data || !data.Instances || !data.Instances.length) {
            return callback();
        }
        collection.mariadb.describeDBInstances[region].data = collection.mariadb.describeDBInstances[region].data.concat(data.Instances);
        return callback();
    };

    let execute = function() {
        client.DescribeDBInstances(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
