const async = require("async");
const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const CamClient = tencentcloud.cam.v20190116.Client;
    const models = tencentcloud.cam.v20190116.Models;
    const Credential = tencentcloud.common.Credential;
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new CamClient(cred, region);
    collection.cam.describeSafeAuthFlagColl[region].data = [];
    async.eachLimit(collection.cam.listUsers[region].data, 10, function(user, bcb){
        let req = new models.DescribeSafeAuthFlagCollRequest();
        req.SubUin = user.Uin;

        let callCB = function(err, data) {
            if (err) {
                collection.cam.describeSafeAuthFlagColl[region].err = err;
                return bcb();
            }
            if (!data) {
                return bcb();
            }

            data.Uin = req.SubUin;
            collection.cam.describeSafeAuthFlagColl[region].data = collection.cam.describeSafeAuthFlagColl[region].data.concat(data);

            return bcb();
        };

        let execute = function() {
            client.DescribeSafeAuthFlagColl(req, function(err, data) {
                if (err) {
                    callCB(err);
                } else {
                    callCB(null, data);
                }
            });
        };
        
        execute();
    }, function() {
        callback();
    });
};
