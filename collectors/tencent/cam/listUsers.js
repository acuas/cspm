const tencentcloud = require("tencentcloud-sdk-nodejs-intl-en");

module.exports = function(TencentConfig, collection, region, callback) {
    const CamClient = tencentcloud.cam.v20190116.Client;
    const models = tencentcloud.cam.v20190116.Models;

    const Credential = tencentcloud.common.Credential;

    // Instantiate an authentication object. The Tencent Cloud account `secretId` and `secretKey` need to be passed in as the input parameters
    let cred = new Credential(TencentConfig.SecretId, TencentConfig.SecretKey);

    const client = new CamClient(cred, region);
    let req = new models.ListUsersRequest();

    collection.cam.listUsers[region].data = [];

    let callCB = function(err, data) {
        if (err) {
            collection.cam.listUsers[region].err = err;
            return callback();
        }
        if (!data || !data.Data || !data.Data.length) {
            return callback();
        }
        collection.cam.listUsers[region].data = collection.cam.listUsers[region].data.concat(data.Data);
        return callback();
    };

    let execute = function() {
        client.ListUsers(req, function(err, data) {
            if (err) {
                callCB(err);
            } else {
                callCB(null, data);
            }
        });
    };

    execute();
};
