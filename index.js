#!/usr/bin/env node

const { ArgumentParser } = require('argparse');
const engine = require('./engine');

console.log("CSPM");

const parser = new ArgumentParser({});

parser.add_argument('--config', {
    help: 'The path to a CSPM config file containing cloud credentials. See config_example.js. If not provided, logic will use default credential chain'
});

parser.add_argument('--compliance', {
    help: 'Compliance mode. Only return results applicable to the selected program.',
    choices: ['hipaa', 'cis', 'cis1', 'cis2', 'pci'],
    action: 'append'
});
parser.add_argument('--plugin', {
    help: 'A specific plugin to run. If none provided, all plugins will be run. Obtain from the exports.js file. E.g. acmValidation'
});
parser.add_argument('--csv', { help: 'Output: CSV file' });
parser.add_argument('--json', { help: 'Output: JSON file' });
parser.add_argument('--junit', { help: 'Output: Junit file' });
parser.add_argument('--console', {
    help: 'Console output format. Default: table',
    choices: ['none', 'text', 'table'],
    default: 'table'
});
parser.add_argument('--collection', { help: 'Output: full collection JSON as file' });
parser.add_argument('--ignore-ok', {
    help: 'Ignore passing (OK) results',
    action: 'store_true'
});
parser.add_argument('--exit-code', {
    help: 'Exits with a non-zero status code if non-passing results are found',
    action: 'store_true'
});
parser.add_argument('--suppress', {
    help: 'Suppress results matching the provided Regex. Format: pluginId:region:resourceId',
    action: 'append'
});
parser.add_argument('--remediate', {
    help: 'Run remediation the provided plugin',
    action: 'append'
});
parser.add_argument('--cloud', {
    help: 'The name of cloud to run plugins for. If not provided, logic will assume cloud from config.js file based on provided credentials',
    choices: ['alibaba', 'tencent'],
    action: 'append'
});

let settings = parser.parse_args();
let cloudConfig = {};

if (!settings.config) {
    console.log('INFO: No config file provided, using default credential chain.');
    return engine(cloudConfig, settings);
}

console.log(`INFO: Using CSPM config file: ${settings.config}`);

try {
    var config = require(settings.config);
} catch (e) {
    console.error('ERROR: Config file could not be loaded. Please ensure you have copied the config_example.js file to config.js');
    process.exit(1);
}

function loadHelperFile(path) {
    try {
        var contents = require(path);
    } catch (e) {
        console.error(`ERROR: The credential file could not be loaded ${path}`);
        console.error(e);
        process.exit(1);
    }
    return contents;
}

function checkRequiredKeys(obj, keys) {
    keys.forEach(function(key){
        if (!obj[key] || !obj[key].length) {
            console.error(`ERROR: The credential config did not contain a valid value for: ${key}`);
            process.exit(1);
        }
    });
}

if (config.credentials.tencent.secret_id && (!settings.cloud || (settings.cloud == 'tencent'))) {
    settings.cloud = 'tencent';
    checkRequiredKeys(config.credentials.tencent, ['secret_id', 'secret_key', 'region']);
    cloudConfig = {
        SecretId: config.credentials.tencent.secret_id,
        SecretKey: config.credentials.tencent.secret_key,
        region: config.credentials.tencent.region
    };
}

else if (config.credentials.alibaba.credential_file && (!settings.cloud || (settings.cloud == 'alibaba'))) {
    settings.cloud = 'alibaba';
    cloudConfig = loadHelperFile(config.credentials.alibaba.credential_file);
} else if (config.credentials.alibaba.access_key_id && (!settings.cloud || (settings.cloud == 'alibaba'))) {
    settings.cloud = 'alibaba';
    checkRequiredKeys(config.credentials.alibaba, ['access_key_secret']);
    cloudConfig = {
        accessKeyId: config.credentials.alibaba.access_key_id,
        accessKeySecret: config.credentials.alibaba.access_key_secret
    };
} 

if (!settings.cloud) {
    console.error('ERROR: Could not determine cloud provider based on provided credentials');
    process.exit(1);
}

console.log(`INFO: Running plugins for: ${settings.cloud}`);

return engine(cloudConfig, settings);
