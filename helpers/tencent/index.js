const regions = require('./regions.js');
const shared = require(__dirname + '/../shared.js');

let helpers = {
    regions: regions,
    MAX_REGIONS_AT_A_TIME: 6,
};

for (let s in shared) helpers[s] = shared[s];

module.exports = helpers;