var calls = {
    IAM: {
        listGroups: {
            property: 'Groups',
            paginate: 'Marker'
        },
    }
};

var postcalls = [
    {
        IAM: {
            getGroup: {
                reliesOnService: 'iam',
                reliesOnCall: 'listGroups',
                filterKey: 'GroupName',
                filterValue: 'GroupName'
            },
        }
    }
];

module.exports = {
    calls: calls,
    postcalls: postcalls,
};