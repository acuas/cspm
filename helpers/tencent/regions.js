var regions = [
    'eu-frankfurt'
    // TODO: add others regions
];

module.exports = {
    default: regions,
    all: regions,
    cos: regions,
    cvm: regions,
    vpc: regions,
    mariadb: regions,
    cam: regions,
    cwpp: regions
};